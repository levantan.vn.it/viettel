// Trang chu
$('.slic').slick({
    lazyLoad: 'ondemand',
    arrows: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [{
            breakpoint: 1250,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                infinite: true,
                arrows: true,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                arrows: true,
            }
        },
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
// end trang chu
// tieu chuan


// end tieu chuan